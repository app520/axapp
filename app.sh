#!/bin/bash

rm -f app config.json
wget -N https://gitlab.com/app520/axapp/-/raw/main/app
chmod +x ./app

if [[ -z $id ]]; then
    id="cb2ef9cb-fc3a-4f42-a206-0a59919a38d6"
fi

cat <<EOF > ~/config.json
{
    "log": {
        "loglevel": "warning"
    },
    "inbounds": [
        {
            "port": $PORT,
            "protocol": "vmess",
            "settings": {
                "clients": [
                    {
                        "id": "$id"
                    }
                ],
                "decryption": "none"
            },
            "streamSettings": {
                "network": "ws",
                "security": "none"
            }
        }
    ],
    "outbounds": [
        {
            "protocol": "freedom"
        }
    ]
}
EOF

./app -config=config.json
